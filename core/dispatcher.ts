/// <reference path='../typings/index.d.ts'/>
/// <reference path='../interfaces.d.ts'/>

import * as http from "http";

type HTTPCallback = (request?: http.IncomingMessage,response?: http.ServerResponse,ctx?: ctx) => string | Object;

//
// NodeJS dispatcher class!
//
export default class Dispatcher {

    public defaultPath: string;
    public listeners : {[key:string] : HTTPCallback } = {};

    constructor(defaultPath?: string) {
        this.defaultPath = defaultPath == undefined ? "" : defaultPath;
        if(this.defaultPath == "/"){
            this.defaultPath = "";
        }
    }

    public use(dispatcher: Dispatcher) {
        if(dispatcher.defaultPath === this.defaultPath) {
            throw new Error('Embed dispatcher have the same default path!');
        }
        else {
            for(let k in dispatcher.listeners) {
                this.listeners[k] = dispatcher.listeners[k];
            }
        }
    }

    public get(path: string,callback: HTTPCallback) {
        if(arguments.length < 2) throw new Error('Invalid numbers of argument');
        this.listeners[`GET:${this.defaultPath}${path}`] = callback;
    }

    public post(path: string,callback: HTTPCallback) {
        if(arguments.length < 2) throw new Error('Invalid numbers of argument');
        this.listeners[`POST:${this.defaultPath}${path}`] = callback;
    }

    public put(path: string,callback: HTTPCallback) {
        if(arguments.length < 2) throw new Error('Invalid numbers of argument');
        this.listeners[`PUT:${this.defaultPath}${path}`] = callback;
    }

    public getRequestBody(request: http.IncomingMessage) : Promise<string> {
        return new Promise<string>( (resolve: (value?: any) => void,reject: (reason?: any) => void) => {
            const body : (Buffer|string)[] = [];
            request.on('data', (chunk : Buffer|string) : void => {
                body.push(chunk);
            })
            .on('error', (err: string) : void => reject(err))
            .on('end',() : void => resolve(Buffer.concat(<Buffer[]>body).toString()));
        });
    }

    public async handle(request: http.IncomingMessage,response: http.ServerResponse) : Promise<routerHandling> {
        return new Promise<routerHandling>( (resolve: (value?: any) => void,reject: (reason?: any) => void) => {
            let httpPath: string = `${request.method}:${request.url}`;
            console.log(`HTTP_Request final path => ${httpPath}`);

            const cb: HTTPCallback = this.listeners[httpPath];
            httpPath = undefined;
            if(cb !== undefined) {
                console.log('HTTP_Request requesting body content!');
                this.getRequestBody(request).then( (body: string) => {
                    console.log('HTTP_Request body required!');
                    const httpRes: string| Object = cb(request,response,{
                        body: body
                    });
                    let template: string = '';
                    let content_type: string = 'text/html';

                    // Resolve final type:
                    switch(typeof httpRes) {
                        case 'object':
                            content_type = 'application/json';
                            template = JSON.stringify(httpRes,null);
                        break;
                        default:
                            template = <string>httpRes;
                        break;
                    }

                    resolve({
                        statusCode: 200,
                        template: template,
                        content_type: content_type
                    });
                });
            }
            else {
                resolve({
                    statusCode: 404,
                    template: "Error 404",
                    content_type: 'text/html'
                });
            }
        });
    }

}
