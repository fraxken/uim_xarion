# UIM_Xarion

A NodeJS / HTML5 CA UIM USM. This custom USM work with the CA UIM Rest API in full asynchrone way.

- Manage alarms
- Manage infrastructure (search for a hub/robot/probe) and interact with it.
- Manage group.

# Installation

> UIM_Xarion use NodeJS v6.6.x

```
npm install
tsd install
ntsc
npm start
```

# Start with embed NodeJS

```
node.exe uim_xarion/server --harmony
```

> Dont forget to put --harmony

# Roadmap

- Refaire le routing avec RIOT
- Faire du parsing JSON en asynchrone.
- [x] Remplacer TSD par typings.
- Ajouter ts au dependencies.
- Faire un tag "tree" json.
- Faire le design de la console d'alarmes.
- Faire le design de groups et infrastructure.
