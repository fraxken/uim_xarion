/// <reference path='../typings/index.d.ts'/>
/// <reference path='../interfaces.d.ts'/>

import Dispatcher from "../core/dispatcher";
import * as http from "http";
import * as pug from "pug";
import * as fs from "fs";

const Router: Dispatcher = new Dispatcher();

Router.get('/',function(request: http.IncomingMessage,response: http.ServerResponse) : string {
    const template : (locals?: any) => string = pug.compileFile('./views/home.pug');
    return template({
        pseudo: "fraxken"
    });
});

Router.post('/getalarms',function(request: http.IncomingMessage,response: http.ServerResponse,ctx: ctx) : string {
    return fs.readFileSync('./data/alarms2.json').toString();
});

Router.post('/getgroups',function(request: http.IncomingMessage,response: http.ServerResponse,ctx: ctx) : string {
    const groups = {};
    const groupsArray = require('./data/groups.json').group;
    groupsArray.forEach( (group: usmGroup) : void => {
        groups[group.groupName] = {};
    });

    return "";
});

const Partials: Dispatcher = new Dispatcher('/partials');

Partials.post('/alarms',function(request: http.IncomingMessage,response: http.ServerResponse) : string {
    const template : (locals?: any) => string = pug.compileFile('./views/partials/alarms.pug');
    return template();
});

Partials.post('/groups',function(request: http.IncomingMessage,response: http.ServerResponse) : string {
    const template : (locals?: any) => string = pug.compileFile('./views/partials/groups.pug');
    return template();
});

Partials.post('/infrastructure',function(request: http.IncomingMessage,response: http.ServerResponse) : string {
    const template : (locals?: any) => string = pug.compileFile('./views/partials/infrastructure.pug');
    return template();
});

Router.use(Partials);

export default Router;
