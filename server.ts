/// <reference path='typings/index.d.ts'/>
/// <reference path='./interfaces.d.ts'/>


import * as http from "http";
import * as cluster from "cluster";
import * as os from "os";
import * as path from "path";
import * as fs from "fs";

const send = require('send');

// Import class
import Dispatcher from "./core/dispatcher";
import Router from "./routers/global";

process.on('unhandledRejection', console.log.bind(console));

// Get number of CPU on the computer!
const numCPUs: number = os.cpus().length;

//
// Set up clustering
//
if (cluster.isMaster) {
    console.time('ClusteringTime');
    let i : number = 0;
    for (; i < 1; i++) {
        cluster.fork();
    }
    console.info('Clustering OK');

    cluster.on('fork', worker => {
        console.log(`Worker n°${worker.process.pid.toString()} is now forked and available for work.`);
    });

    cluster.on('exit', (worker: cluster.Worker, code: any, signal: any) => {
        console.log(`Worker n°${worker.process.pid.toString()} is dead !`);
        console.log('Restarting ...');
        cluster.fork();
    });
    console.timeEnd('ClusteringTime');
}
else {

    // Setup http server
    const server : http.Server = http.createServer((request: http.IncomingMessage,response:http.ServerResponse) : void => {
        console.time('HTTP_Request');

        // Audit trace
        console.log(`Incomming request on ${request.url} with method => ${request.method}`);

        // Detect if the request if for a routing handling or a file handling!
        let ext : string = path.extname(request.url);
        if(ext !== undefined && ext !== "") {
            console.log(`Serving static asset!`);
            send(request, request.url,{root: './static'}).pipe(response);
            console.timeEnd('HTTP_Request');
        }
        else {
            console.log("Handling HTTP Request as normal route!");
            Router.handle(request,response).then( (header: routerHandling) => {
                response.setHeader('Content-Type',header.content_type);
                response.statusCode = header.statusCode;
                response.end(header.template);
                console.timeEnd('HTTP_Request');
            });
        }
    });

    // Listen port
    server.listen(3000);

    // Close & Exception handling
    const closeHandler : () => void = () => {
        server.close();
        process.exit();
    }
    process.on('exit',closeHandler);
    process.on('SIGINT',closeHandler);
    process.on('uncaughtException',closeHandler);

}
