<alarms-console>
    <h1>Alarms console</h1>
    <section id="toolsbar">
        <label>Filtrage des alarmes : </label>
        <input id="filtre" name="filtre" placeholder='Entre ton filtre ici!' />
    </section>
    <section id="alarms" name="alarms_container">

    </section>

    <script>
        var alarms_container;
        this.on('mount', () => {
            alarms_container = this.root.querySelector('#alarms');
        });
        var xhr = new XMLHttpRequest();
        xhr.open("POST","/getalarms",true);
        xhr.send();
        xhr.onreadystatechange = (evt) => {
            if(xhr.readyState == 4) {
                if(xhr.status == 200) {
                    console.time('parsingAlarms');
                    let alarms = JSON.parse(xhr.responseText).alarm;
                    console.timeEnd('parsingAlarms');
                    console.time('loadAlarms');
                    var i = 0;
                    var length = alarms.length;
                    var fragmentContainer = document.createDocumentFragment();
                    for(;i < length;i++) {
                        var el = document.createElement('section');
                        el.className = 'alarms';
                        el.classList.add(alarms[i].severity);

                        var top = document.createElement('section');
                        top.className = 'top';

                        var serverSection = document.createElement('section');
                        serverSection.className = 'server';
                        serverSection.innerHTML = '<p>'+alarms[i].hostname+'</p><span class="'+alarms[i].severity+'">'+alarms[i].severity+'</span>';

                        var infoSection = document.createElement('section');
                        infoSection.className = 'info';
                        infoSection.innerHTML = '<p><b>Origin</b> : '+alarms[i].origin.join('|')+'  -  <b>Usertag2</b> : '+alarms[i].userTag2+'</p>';

                        top.appendChild(serverSection);
                        top.appendChild(infoSection);

                        //el.innerHTML = alarms[i].message;
                        var p = document.createElement('p');
                        p.className = 'message';
                        p.textContent = (alarms[i].message.length > 140) ? alarms[i].message.substr(0,140)+"..." : alarms[i].message;
                        el.appendChild(top);
                        el.appendChild(p);
                        fragmentContainer.appendChild(el);
                    }
                    alarms_container.innerHTML = "";
                    alarms_container.appendChild(fragmentContainer);
                    console.timeEnd('loadAlarms');
                }
                else {
                    console.log("Impossible de récupérer la liste des alarmes!");
                }
            }
        }
        xhr.onerror = function(e) {
            console.log(e.target.status);
        }
    </script>

    <style scoped>
        section#toolsbar {
            height: 30px;
            position: relative;
            margin-top: 10px;
            flex-shrink: 0;
            display: flex;
        }
            section#toolsbar > label {
                display: flex;
                align-items: center;
                margin-right: 10px;
                padding: 0 10px;
                color: #37474F;
            }
            section#toolsbar > input {
                border-radius: 2px;
                outline: none;
                box-shadow: 1px 1px 6px rgba(20,20,20,0.05) inset;
                border: 2px solid #CCC;
                padding: 0 10px;
            }
        section#alarms {
            display: flex;
            margin-top: 10px;
            flex-direction: column;
            position: relative;
            width: 100%;
        }
            section#alarms > .alarms {
                flex-basis: 60px;
                height: 60px;
                box-sizing: border-box;
                /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#ffffff+0,e5e5e5+100;White+3D */
                background: rgb(255,255,255); /* Old browsers */
                background: -moz-linear-gradient(top,  rgba(255,255,255,1) 0%, rgba(229,229,229,1) 100%); /* FF3.6-15 */
                background: -webkit-linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(229,229,229,1) 100%); /* Chrome10-25,Safari5.1-6 */
                background: linear-gradient(to bottom,  rgba(255,255,255,1) 0%,rgba(229,229,229,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#e5e5e5',GradientType=0 ); /* IE6-9 */
                overflow: hidden;
                flex-shrink: 0;
                border-radius: 2px;
                padding-left: 10px;
                display: flex;
                flex-direction: column;
            }
                section#alarms > .alarms:hover {
                    background: #EEE;
                    cursor: pointer;
                }
                section#alarms > .alarms > .top {
                    height: 20px;
                    margin-top: 6px;
                    display: flex;
                    font-size: 15px;
                }
                    section#alarms > .alarms > .top > .server {
                        border-radius: 2px;
                        background: #CFD8DC;
                        color: #333;
                        font-weight: bold;
                        padding-left: 10px;
                        display: flex;
                        align-items: center;
                        flex-basis: 300px;
                    }
                        section#alarms > .alarms > .top > .server > span {
                            margin-left: auto;
                            height: 20px;
                            display: flex;
                            align-items: center;
                            padding: 0 10px;
                            border-radius: 2px 0 0 2px;
                            width: 100px;
                        }
                            section#alarms > .alarms > .top > .server > span.critical {
                                background: #F44336;
                                color: #FFF;
                            }
                            section#alarms > .alarms > .top > .server > span.major {
                                background: #FFA726;
                                color: #FFF;
                            }
                            section#alarms > .alarms > .top > .server > span.minor {
                                background: #FFEB3B;
                                color: #FFF;
                            }
                            section#alarms > .alarms > .top > .server > span.warning {
                                background: #2196F3;
                                color: #FFF;
                            }
                            section#alarms > .alarms > .top > .server > span.information {
                                background: #8BC34A;
                                color: #FFF;
                            }

                    section#alarms > .alarms > .top > .info {
                        display: flex;
                        align-items: center;
                        font-size: 14px;
                        margin-left: auto;
                        margin-right: 20px;
                    }

                section#alarms > .alarms > p.message {
                    font-size: 15px;
                    color: #212121;
                    margin-top: 5px;
                    font-weight: bold;
                }

                section#alarms > .alarms.critical {
                    border-left: 5px solid #F44336;
                }
                section#alarms > .alarms.major {
                    border-left: 5px solid #FFA726;
                }
                section#alarms > .alarms.minor {
                    border-left: 5px solid #FFEB3B;
                }
                section#alarms > .alarms.information {
                    border-left: 5px solid #8BC34A;
                }
                section#alarms > .alarms.warning {
                    border-left: 5px solid #2196F3;
                }

            section#alarms > .alarms + .alarms {
                margin-top: 5px;
            }
    </style>
</alarms-console>
