<tree>
    <h2>{ opts.title }</h2>
    <section class="tree"></section>

    <script>

        var containerHandler = function(e) {
            var focus_ul = e.target.parentNode.parentNode.childNodes[1];
            focus_ul.classList.toggle('hidden');
        }

        // Tree Generator
        var treeGenerator = function(data,deep) {
            var subFragment = function(obj,le) {
                if(obj instanceof Array) {
                    var ULElement = document.createElement('ul');
                    for(var key in obj) {
                        var LIElement = document.createElement('li');

                        var ParagrapheElement = document.createElement('p');
                        ParagrapheElement.appendChild(document.createTextNode(obj[key]));
                        LIElement.appendChild(ParagrapheElement);

                        LIElement.className = 'arrayElement';
                        //LIElement.addEventListener('click',containerHandler);
                        ULElement.appendChild(LIElement);
                    }
                    (le === 2) && ULElement.classList.add('hidden');
                    return ULElement;
                }
                else if(typeof obj == 'object') {
                    var ULElement = document.createElement('ul');
                    for(var key in obj) {
                        var LIElement = document.createElement('li');
                        LIElement.className='container';

                        var ParagrapheElement = document.createElement('p');
                        ParagrapheElement.className='title';
                        var SpanElement = document.createElement('span');
                        SpanElement.appendChild(document.createTextNode('+'));
                        SpanElement.addEventListener('click',containerHandler);

                        ParagrapheElement.appendChild(SpanElement);
                        ParagrapheElement.appendChild(document.createTextNode(key));

                        LIElement.appendChild(ParagrapheElement);
                        LIElement.appendChild(subFragment(obj[key],2));
                        (le === 2) && ULElement.classList.add('hidden');
                        ULElement.appendChild(LIElement);
                    }
                    return ULElement;
                }
                else {
                    var ParagrapheElement = document.createElement('p');
                    ParagrapheElement.appendChild(document.createTextNode(obj.toString()));
                    return ParagrapheElement;
                }
            }
            return subFragment(data,deep);
        }
        // On mount!
        this.on('mount',function() {
            console.time('mountTree_'+opts.title);
            var HTMLElement = treeGenerator(this.opts.data,1);
            this.root.querySelector('.tree').appendChild(HTMLElement);
            console.timeEnd('mountTree_'+opts.title);
        });
    </script>

    <style scoped>
        h2 {
            margin-bottom: 20px;
        }
        .tree {
            display: flex;
            flex-direction: column;
        }
        .tree > ul {
            display: flex;
            flex-direction: column;
        }
        .tree > ul > li + li {
            margin-top: 10px;
        }
        .tree > ul > li ul > li + li {
            margin-top: 3px;
        }
        .tree li.arrayElement {
            justify-content: flex-start;
        }
        .tree ul.hidden li {
            display: none;
        }
        .tree li > p.title > span {
            background: #1565C0;
            color: #FFF;
            width: 16px;
            height: 16px;
            display: flex;
            align-items: center;
            justify-content: center;
            border-radius: 2px;
            margin-right: 10px;
            line-height: 0;
            user-select: none;
        }
        .tree li > p.title {
            background: #F5F5F5;
            margin-bottom: 10px;
            color: #333;
            height: 24px;
            display: flex;
            align-items: center;
            border-radius: 2px;
            padding-left: 10px;
        }
            .tree li > p.title:hover {
                background: #EEE;
                cursor: pointer;
            }
        .tree li > p:not(.title) {
            height: 22px;
            display: flex;
            align-items: center;
        }
            .tree li > p:not(.title):hover {
                font-weight: bold;
                cursor: pointer;
            }
        .tree li > ul {
            margin-left: 20px;
        }
    </style>
</tree>
