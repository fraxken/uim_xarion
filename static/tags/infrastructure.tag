<usm-infrastructure>
    <tree title="Hubs" data={data} name="infrastructure_tree"></tree>
    <section id="container"></section>

    <script>
        var container;
        this.on('mount',function() {
            container = this.root.querySelector('#container');
        });
        this.data = {
            "test" : {
                "lol" : [
                    "h1","h2"
                ]
            },
            "salut": {
                "empty" : {
                    "yop" : {
                        "test" : [ "test" , "test2"]
                    },
                    "xd" : [ "mdr" , "xd" ]
                }
            }
        };
    </script>

    <style scoped>
        tree {
            width: 30%;
            display: flex;
            flex-direction: column;
            margin-right: 20px;
            box-sizing: padding-box;
            padding: 20px;
        }
        section#container {
            flex-grow: 1;
            display: flex;
            flex-direction: column;
            background: red;
        }
    </style>
</usm-infrastructure>
