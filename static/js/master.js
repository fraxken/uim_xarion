document.addEventListener("DOMContentLoaded", function(event) {
    console.log("DOM fully loaded and parsed");

    //
    // Get pages elements!
    //
    var main                = document.getElementById('main');
    var menu_alarms         = document.getElementById('menu_alarms');
    var menu_groups         = document.getElementById('menu_groups');
    var menu_infrastructure = document.getElementById('menu_infrastructure');

    var active = menu_alarms;

    var httpRequest = function(path,type,cb) {
        console.log("send http request at "+path);
        var xhr = new XMLHttpRequest();
        xhr.open(type,path,true);
        xhr.send();
        xhr.onreadystatechange = function(evt) {
            xhr.readyState == 4 && cb(xhr,evt);
        }
        xhr.onerror = function(e) {
            console.log(e.target.status);
        }
    }
    httpRequest('/partials/alarms','POST',function(xhr,evt) {
        if(xhr.status == 200) {
            main.innerHTML = xhr.responseText;
            riot.mount('alarms-console');
        }
        else {
            main.innerHTML = "<h1>Erreur - Impossible de charger le HTML!</h1>";
        }
    });

    //
    // Configure menu behavior
    //
    menu_alarms.addEventListener('click',function(e) {
        if(active != e.target) {
            httpRequest('/partials/alarms','POST',function(xhr,evt) {
                if(xhr.status == 200) {
                    active.classList.remove('active');
                    e.target.classList.add('active');
                    active = e.target;
                    main.innerHTML = xhr.responseText;
                    riot.mount('alarms-console');
                }
                else {
                    main.innerHTML = "<h1>Erreur - Impossible de charger le HTML!</h1>";
                }
            });
        }
    });

    menu_groups.addEventListener('click',function(e) {
        if(active != e.target) {
            httpRequest('/partials/groups','POST',function(xhr,evt) {
                if(xhr.status == 200) {
                    active.classList.remove('active');
                    e.target.classList.add('active');
                    active = e.target;
                    main.innerHTML = xhr.responseText;
                    riot.mount('usm-groups');
                }
                else {
                    main.innerHTML = "<h1>Erreur - Impossible de charger le HTML!</h1>";
                }
            });
        }
    });

    menu_infrastructure.addEventListener('click',function(e) {
        if(active != e.target) {
            httpRequest('/partials/infrastructure','POST',function(xhr,evt) {
                if(xhr.status == 200) {
                    active.classList.remove('active');
                    e.target.classList.add('active');
                    active = e.target;
                    main.innerHTML = xhr.responseText;
                    riot.mount('usm-infrastructure');
                }
                else {
                    main.innerHTML = "<h1>Erreur - Impossible de charger le HTML!</h1>";
                }
            });
        }
    });


});
