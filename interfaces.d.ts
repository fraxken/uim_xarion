interface routerHandling {
    statusCode: number;
    content_type: string;
    template: string;
}

interface usmGroup {
    groupType: string;
    groupId: string;
    parentGroupId: string;
    groupName: string;
    desc: string;
    descToken: string;
    priority: string;
    lastUpdate: string;
    active: string;
}

interface usmGroupMember {
    csId: string;
    csName: string;
    csIp: string;
    groupId: string;
}

interface ctx {
    body: any;
}
